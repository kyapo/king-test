#!/usr/bin/env sh

# Miniconda command access
export MLFLOW_CONDA_HOME=/home/payi/miniconda3
export PATH=$PATH:$MLFLOW_CONDA_HOME/condabin

# Adress of the tracking server where the Model Registry resides
export MLFLOW_TRACKING_URI=http://localhost:5000

# Parameters of the model REST API service
export MLFLOW_SERVICE_HOST=0.0.0.0
export MLFLOW_SERVICE_PORT=8001

# Serve the production model from the model registry
mlflow models serve -m 'runs:/b70d83a4f038438cb7d61e63e33acc6e/model' \
                    -h $MLFLOW_SERVICE_HOST \
                    -p $MLFLOW_SERVICE_PORT

# use : ./service.sh
