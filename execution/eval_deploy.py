import os
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import mlflow

mlflow.set_tracking_uri("postgresql+psycopg2://mlflow:mlflow@localhost/mlflow")

logged_model = 'runs:/b70d83a4f038438cb7d61e63e33acc6e/model'

# Load model as a PyFuncModel.
loaded_model = mlflow.pyfunc.load_model(logged_model)

# Load DataFrame
wine_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "wine-quality.csv")
data = pd.read_csv(wine_path)
train, test = train_test_split(data)
test_x = test.drop(["quality"], axis=1)


# Predict on a Pandas DataFrame.
result = loaded_model.predict(pd.DataFrame(test_x))

print(result)
