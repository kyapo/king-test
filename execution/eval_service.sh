#!/usr/bin/env sh

curl http://127.0.0.1:8001/invocations \
     -H 'Content-Type:application/json' \
     -d '{ "columns": ["fixed acidity","volatile acidity","citric acid",
                       "residual sugar","chlorides","free sulfur dioxide",
                       "total sulfur dioxide","density","pH","sulphates","alcohol"],
          "data": [[7,0.27,0.36,20.7,0.045,45,170,1.001,3,0.45,8.8],
                   [7.5,2.7,0.3,22,0.45,40.5,170,1.001,3.6,1.2,9.8]]}'

